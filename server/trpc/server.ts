import { appRouter } from "@/lib/trpc/routers"
import { createCallerFactory } from "@/lib/trpc/trpc";

export const createCaller = createCallerFactory(appRouter);
