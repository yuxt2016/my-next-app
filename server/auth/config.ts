import { getRequestParams } from '@/lib/utils/help';
import type { NextAuthConfig } from 'next-auth';

export const authConfig = {
  pages: {
    signIn: '/login',
  },
  providers: [
    // added later in auth.ts since it requires bcrypt which is only compatible with Node.js
    // while this file is also used in non-Node.js environments
  ],
  callbacks: {
    redirect({ url, baseUrl }) {
      const { callbackUrl } = getRequestParams(url);
      return callbackUrl || baseUrl;
    },
    session({ session }) {
      // console.log("session ==", session);
      return session;
    },
    authorized({ auth, request: { nextUrl } }) {
      // console.log("=========================================================");
      // console.log("auth user = ", auth?.user);
      // console.log("auth url = ", nextUrl.pathname);
      const isLoggedIn = !!auth?.user;
      if (isLoggedIn) {
        // 跳转登录路由则转发首页
        if(nextUrl.pathname == '/login') {
          return Response.redirect(new URL('/', nextUrl));
        }
        return true;
      }
      return false;
    },
  },
} satisfies NextAuthConfig;
