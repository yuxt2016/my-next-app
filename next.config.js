/** @type {import('next').NextConfig} */
// 判断环境
const isProd = ['production'].includes(process.env.NODE_ENV);
const nextConfig = {
	// distDir: '.next',
	// output: "export",
	// images: { unoptimized: true },
	compress: false,
	trailingSlash: true,
	reactStrictMode: false, // 严格模式
	skipTrailingSlashRedirect: true, // 允许禁用 Next.js 默认重定向以添加或删除尾部斜杠，从而允许在中间件内部进行自定义处理，这可以允许为某些路径维护尾部斜杠，但不允许为其他路径维护尾部斜线，从而允许更容易的增量迁移。
	// skipMiddlewareUrlNormalize: true, // 允许禁用规范 Next.js 的 URL，以使处理直接访问和客户端转换相同。在一些高级情况下，您需要使用解锁的原始 URL 进行完全控制。
	serverRuntimeConfig: {
		mySecret: 'secret2222',
	},
	publicRuntimeConfig: {
		// Will be available on both server and client
		staticFolder: '/static',
	},
	// async rewrites() {
	// 	if (!isProd) {
	// 		return [
	// 			{
	// 				source: '/api/:slug*',
	// 				destination: process.env.PROXY,
	// 			},
	// 		];
	// 	} else {
	// 		return [];
	// 	}
	// },
};

const withNextIntl = require("next-intl/plugin")("./lib/i18n/index.ts");
module.exports = withNextIntl(nextConfig)

// module.exports = nextConfig
