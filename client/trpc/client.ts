import { httpBatchLink } from '@trpc/client';
import { createTRPCNext } from '@trpc/next';
import { type AppRouter } from '@/lib/trpc/routers';

export const client = createTRPCNext<AppRouter>({
    config(opts) {
        return {
            links: [
                httpBatchLink({
                    url: `http://localhost:3000/api/trpc`,
                    async headers() {
                        return {
                            // authorization: null,
                        };
                    },
                }),
            ],
        };
    },
    ssr: false
});

// import { fetchRequestHandler } from '@trpc/server/adapters/fetch';
// import { appRouter } from '@/trpc/routers';
// import { createContext } from '@/trpc/context';

// const handler = (req: Request) =>
//     fetchRequestHandler({
//         endpoint: '/api/trpc',
//         req,
//         router: appRouter,
//         createContext
//     });
// export { handler as GET, handler as POST };
