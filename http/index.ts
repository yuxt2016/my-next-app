// api/index.ts
import { generatorAPIS, request } from "@/lib/utils/request";

// 方式一
export const getChatflows = () => {
    return request({
        url: "http://localhost:3000/api/v1/chatflows",
        method: "GET",
    });
}

// 方式二 这样省资源
enum IndexApis {
    getChatflows = 'GET api/v1/chatflows"',
}

export default generatorAPIS<typeof IndexApis>(IndexApis);