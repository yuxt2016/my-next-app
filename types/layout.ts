import { ReactNode } from 'react';

interface PropsParams {
    locale: string
}

export interface LayoutProps {
    children: ReactNode
    params: Partial<PropsParams>
}