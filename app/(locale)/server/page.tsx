"use server";
import fs from 'fs'
import { cwd } from 'process';
import React from 'react';
import os from 'os'
import getConfig from 'next/config';

const { serverRuntimeConfig, publicRuntimeConfig } = getConfig();

function ServerPage() {
  const paths = fs.readdirSync(cwd());
  return (
    <>
      <div>内存使用率{(os.freemem() / os.totalmem()).toFixed(2)}</div>
      <div>serverRuntimeConfig: {JSON.stringify(serverRuntimeConfig)}</div>
      <div>publicRuntimeConfig: {JSON.stringify(publicRuntimeConfig)}</div>
      <div>
        {paths.map(_ => <div key={_}>{_}</div>)}
      </div>
    </>
  )
}

export default ServerPage;
