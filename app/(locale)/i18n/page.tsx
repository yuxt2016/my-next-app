// 'use client';
// import { useTranslations } from 'next-intl';
// import { getTranslations } from 'next-intl/server';
// import LangSwitcher from '@/app/ui/langSwitcher';

export default async function Page() {
    // const t = await getTranslations(); // 服务端
    // const t = useTranslations(); // 客户端

    return (
        <main className="flex min-h-screen flex-col p-6">
            <h3>This is a i18n demo page</h3>
            {/* <LangSwitcher /> */}
            {/* <div className='m-10 p-2'> {t("title")}</div> */}
        </main>
    );
}
