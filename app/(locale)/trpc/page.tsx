'use client';
import { client } from '@/client/trpc/client';
import { createCaller } from '@/server/trpc/server';
import { createContextInner } from '@/lib/trpc/context';

function Page() {
    // 客户端
    const { data } = client.authRouter.getInfo.useQuery(undefined, { retry: 0 });
    console.log("data == ", data);

    // 服务端
    // const ctx = await createContextInner();
    // const data = await createCaller(ctx).authRouter.getInfo();
    // console.log("data == ", data);

    return (
        <main className="flex min-h-screen flex-col p-6">
            <h3>This is a trpc demo page</h3>
            <h3>=========================</h3>
            <h3>data: {JSON.stringify(data)}</h3>
        </main>
    );
}

export default client.withTRPC(Page);