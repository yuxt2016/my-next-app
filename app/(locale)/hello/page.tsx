"use client";
import useSettingStore from '@/lib/store/useSettingStore';

export default function HelloPage() {
  const title = useSettingStore((state) => state.title);
    return <div>Hello {title}</div>
}