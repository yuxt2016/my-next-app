"use client";
import { Toaster } from "@/app/ui/components/ui/toaster";
import { Dialog, DialogContent, DialogFooter, DialogHeader } from '@/app/ui/components/ui/dialog';
import useDialogStore from "@/lib/store/useDialogStore";
import { DialogTitle } from "@radix-ui/react-dialog";
import { Button } from "@/app/ui/components/ui/button";

export default function Layout({ children }: { children: React.ReactNode }) {
    const { open, showCancelBtn, showOkBtn, cancelBtnClassName, okBtnClassName, title, content, onCancel, onOk, setOpen } = useDialogStore();
    return (
        <>
            {children}
            <Toaster />
            <Dialog open={open} modal onOpenChange={setOpen} defaultOpen={open}>
                <DialogContent className="sm:max-w-[425px]">
                    <DialogHeader>
                        <DialogTitle>{title}</DialogTitle>
                    </DialogHeader>
                    {content}
                    <DialogFooter className="flex gap-2 justify-end">
                        {
                            showOkBtn && <Button variant='default' className={okBtnClassName} onClick={onOk}>确认</Button>
                        }
                        {
                            showCancelBtn && <Button variant="outline" className={cancelBtnClassName} onClick={onCancel}>取消</Button>
                        }
                    </DialogFooter>
                </DialogContent>
            </Dialog>
        </>
    );
}
