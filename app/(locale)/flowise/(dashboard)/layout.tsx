"use client";
import SideNav from '@/app/ui/flowise/sidenav';
import Header from '@/app/ui/flowise/header';
import { useState } from 'react';

export default function Layout({ children }: { children: React.ReactNode }) {
    const [toggle, setToggle] = useState<boolean>(true);
    return (
        <div className="flex h-screen flex-col overflow-hidden">
            <Header onToggle={() => { setToggle(!toggle) }}/>
            <div className="h-full flex flex-row">
                <SideNav toggle={toggle}/>
                <div className="flex-1 grow p-4 overflow-y-auto">{children}</div>
            </div>
        </div>
    );
}
