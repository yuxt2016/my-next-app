import { ChatWindow } from "@/app/ui/chat/ChatWindow";

export default function OutputPage() {
  const InfoCard = (
    <div className="p-4 md:p-8 rounded bg-[#25252d] w-full max-h-[85%] overflow-hidden">
      <h1 className="text-xl md:text-2xl mb-4">
        🦜 问答输出结构化JSON内容 🦜
      </h1>
    </div>
  );
  return (
    <ChatWindow
      endpoint="/api/chat/structured_output"
      emptyStateComponent={InfoCard}
      placeholder="无论你在这里输入什么，都会返回相同结构的JSON对象!"
      emoji="🧱"
      titleText="结构化输出"
    ></ChatWindow>
  );
}
