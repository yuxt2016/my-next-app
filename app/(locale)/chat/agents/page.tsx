import { ChatWindow } from "@/app/ui/chat/ChatWindow";

export default function AgentsPage() {
  const InfoCard = (
    <div className="p-4 md:p-8 rounded bg-[#25252d] w-full max-h-[85%] overflow-hidden">
      <h1 className="text-3xl md:text-4xl mb-4">
      🦜 Agents 允许开发者构建复杂的、多任务的 AI 助手和对话系统 🦜
      </h1>
    </div>
  );
  return (
    <ChatWindow
      endpoint="/api/chat/agents"
      emptyStateComponent={InfoCard}
      placeholder="我是一个会话代理!"
      titleText="请输入问题"
      emoji="🦜"
      showIntermediateStepsToggle={true}
    ></ChatWindow>
  );
}
