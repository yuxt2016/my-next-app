import { ChatWindow } from "@/app/ui/chat/ChatWindow";

export default function RetrievalPage() {
  const InfoCard = (
    <div className="p-4 md:p-8 rounded bg-[#25252d] w-full max-h-[85%] overflow-hidden">
      <h1 className="text-xl md:text-2xl mb-4">
        🦜 文档增强检索回答问题 🦜
      </h1>
    </div>
  );
  return (
    <ChatWindow
      endpoint="/api/chat/retrieval"
      emptyStateComponent={InfoCard}
      placeholder="根据文档回答问题!"
      emoji="🧱"
      titleText="文档增强检索"
    ></ChatWindow>
  );
}
