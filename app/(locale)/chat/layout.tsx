import { Navbar } from "@/app/ui/chat/Navbar";

export default function Layout({
    children,
}: {
    children: React.ReactNode;
}) {
    return (
        <div className="flex flex-col bg-slate-700 text-white p-4 md:p-12 h-[100vh]">
            <Navbar></Navbar>
            {children}
        </div>
    );
}
