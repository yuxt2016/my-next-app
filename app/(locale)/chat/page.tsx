import { ChatWindow } from "@/app/ui/chat/ChatWindow";

export default function ChatPage() {
    const InfoCard = (
        <div className="p-4 md:p-8 rounded bg-[#25252d] w-full max-h-[85%] overflow-hidden">
            <h1 className="text-xl md:text-2xl mb-4">
                🦜 通过问答输出会话内容 🦜
            </h1>
        </div>
    );
    return (
        <ChatWindow
            endpoint="/api/chat"
            emoji="🤖"
            titleText="会话"
            placeholder="基于gpt-3.5-turbo回答问题"
            emptyStateComponent={InfoCard}
        ></ChatWindow>
    );
}
