import '@/app/ui/global.css';
import { inter } from '@/app/ui/fonts';
import { Metadata } from 'next';
// 国际化
// import { NextIntlClientProvider, useMessages } from 'next-intl';
import { LayoutProps } from '@/types/layout';

export const metadata: Metadata = {
  title: {
    template: '%s | Hello Next',
    default: 'Hello Next',
  },
  description: 'The official Next.js Learn Dashboard built with App Router.',
  metadataBase: new URL('https://next-learn-dashboard.vercel.sh')
};

export default function RootLayout({ children, params }: LayoutProps) {
  // const messages = useMessages(); // 国际化消息
  // const { locale } = params; // 国际化语言

  return (
    <html lang="en">
      {/* <NextIntlClientProvider locale={locale} messages={messages}> */}
        <body className={`${inter.className} antialiased`}>
          {children}
        </body>
      {/* </NextIntlClientProvider> */}
    </html>
  );
}
