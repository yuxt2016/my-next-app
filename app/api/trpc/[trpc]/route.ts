import { appRouter } from '@/lib/trpc/routers';
import { fetchRequestHandler } from '@trpc/server/adapters/fetch';

export const generateStaticParams = () => {
    return [
        {
            trpc: "authRouter.getInfo"
        },
        {
            trpc: "authRouter.login"
        },
    ];
}

// https://trpc.nodejs.cn/docs/server/adapters/nextjs
const handler = (req: Request) =>
    fetchRequestHandler({
        endpoint: '/api/trpc',
        req,
        router: appRouter,
        createContext: () => ({} as any)
    });
export { handler as GET, handler as POST };