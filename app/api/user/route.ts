import { getUser } from "@/server/db/data";
import { NextResponse, NextRequest } from "next/server";

/**
 * @description 根据email获取用户
 * @param {string} email 电子邮箱 eg: http://localhost:3000/api/user?email=user@nextmail.com
 * @return;
 * */
export async function GET(request: NextRequest) {
    try {
        const email = request.nextUrl.searchParams.get("email");
        const user = await getUser(email as string);
        return NextResponse.json({
            code: 0,
            data: user
        });
    } catch (error) {
        return NextResponse.json({
            code: -1,
            msg: error
        });
    }
}
