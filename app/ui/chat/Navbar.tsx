"use client";

import { usePathname } from 'next/navigation';

export function Navbar() {
  const pathname = usePathname();
  return (
    <nav className="mb-4 font-bold text-lg">
      <a className={`mr-4 ${pathname === "/chat" ? "text-blue-400 border-b" : ""}`} href="/chat">🤖 会话</a>
      <a className={`mr-4 ${pathname === "/chat/structured_output" ? "text-blue-400 border-b" : ""}`} href="/chat/structured_output">🧱 结构化</a>
      <a className={`mr-4 ${pathname === "/chat/retrieval" ? "text-blue-400 border-b" : ""}`} href="/chat/retrieval">🧱 增强文档</a>
      <a className={`mr-4 ${pathname === "/chat/agents" ? "text-blue-400 border-b" : ""}`} href="/chat/agents">🧱 代理</a>
    </nav>
  );
}