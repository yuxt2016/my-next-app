"use client";
import { MessageSquareText, X } from "lucide-react";
import { Button } from "../components/ui/button";
import { Popover, PopoverContent, PopoverTrigger } from "../components/ui/popover";
import { useState } from "react";
import { ChatWindow } from "../chat/ChatWindow";
export default function ShowMessage() {
    const [show, setShow] = useState<boolean>(false);

    return <>
        <Popover onOpenChange={setShow}>
            <PopoverTrigger asChild>
                <Button size="icon" className="rounded-full shadow-2xl bg-secondary" onClick={() => setShow(!show)}>
                    {show ? <X className="h-5 w-5 text-white" /> : <MessageSquareText className="h-5 w-5 text-white" />}
                </Button>
            </PopoverTrigger>
            <PopoverContent className="w-[465px] overflow-hidden p-0 rounded-md shadow-xl">
                <ChatWindow
                    endpoint="/api/chat"
                    emoji="🤖"
                    titleText="会话"
                    placeholder="基于gpt-3.5-turbo回答问题"
                    emptyStateComponent={<div className="w-full min-h-14"></div>}
                ></ChatWindow>
            </PopoverContent>
        </Popover >
    </>
}