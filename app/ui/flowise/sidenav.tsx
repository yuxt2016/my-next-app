"use client";
import clsx from 'clsx';
import { Bot, BotMessageSquare, FileText, Kanban, Key, Presentation, Users, Variable, Wrench } from 'lucide-react';
import Link from 'next/link';
import { usePathname } from 'next/navigation';

const MenuList = [
    {
        id: 'chatflows',
        title: '聊天流',
        type: 'item',
        url: '/',
        icon: BotMessageSquare,
        breadcrumbs: true
    },
    {
        id: 'agentflows',
        title: '代理流',
        type: 'item',
        url: '/agentflows',
        icon: Kanban,
        breadcrumbs: true,
        isBeta: true
    },
    {
        id: 'marketplaces',
        title: '市场',
        type: 'item',
        url: '/marketplaces',
        icon: Presentation,
        breadcrumbs: true
    },
    {
        id: 'tools',
        title: '工具',
        type: 'item',
        url: '/tools',
        icon: Wrench,
        breadcrumbs: true
    },
    {
        id: 'assistants',
        title: '助理',
        type: 'item',
        url: '/assistants',
        icon: Bot,
        breadcrumbs: true
    },
    {
        id: 'credentials',
        title: '凭证',
        type: 'item',
        url: '/credentials',
        icon: Users,
        breadcrumbs: true
    },
    {
        id: 'variables',
        title: '变量',
        type: 'item',
        url: '/variables',
        icon: Variable,
        breadcrumbs: true
    },
    {
        id: 'apikey',
        title: 'API密钥',
        type: 'item',
        url: '/apikey',
        icon: Key,
        breadcrumbs: true
    },
    {
        id: 'document-stores',
        title: '文档存储',
        type: 'item',
        url: '/document-stores',
        icon: FileText,
        breadcrumbs: true
    }
]

export default function SideNav({ toggle }: { toggle: boolean }) {
    const pathname = usePathname();
    const toggleClassName = toggle ? 'w-[255px]' : "w-0";
    return (
        <div className={clsx([toggleClassName,'transition-all ease-in-out overflow-hidden'])} >
            <div className='flex h-full flex-col w-full px-2.5 py-4 border-r border-[#90caf9]'>
                {
                    MenuList.map(i => {
                        const isActive = pathname.includes(i.url);
                        return <Link key={i.id} className={clsx(['flex items-center whitespace-nowrap gap-2 mb-1 justify-start rounded-xl p-4 hover:bg-[#ede7f6] group', isActive && 'bg-[#ede7f6]'])} href={i.url}>
                            <i.icon className={clsx(["h-4 w-4 group-hover:text-[#5e35b1]", isActive && 'text-[#5e35b1]'])} />
                            <div className={clsx(['group-hover:text-[#5e35b1] text-sm', isActive && 'text-[#5e35b1]'])}>{i.title}</div>
                        </Link>
                    })
                }
            </div>
        </div>
    );
}
