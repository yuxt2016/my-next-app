"use client";
import {
    AlignJustify,
    Info,
    LogOut,
    Settings,
    User,
} from "lucide-react"
import {
    DropdownMenu,
    DropdownMenuContent,
    DropdownMenuItem,
    DropdownMenuLabel,
    DropdownMenuSeparator,
    DropdownMenuTrigger
} from "../components/ui/dropdown-menu";
import { Label } from "../components/ui/label";
import { Switch } from "../components/ui/switch";
import { Button } from "../components/ui/button";
import Image from 'next/image';

type HeaderProps = {
    onToggle?: () => void
}

export default function Header({ onToggle }: HeaderProps) {
    return (
        <div className="flex items-center px-5 py-2 border-b border-[#90caf9]">
            <div className="flex items-center justify-between w-[230px] whitespace-nowrap">
                <Image
                    src="/flowise_logo.png"
                    height={48}
                    width={150}
                    alt="flowise_logo"
                />
                <Button variant="outline" size="icon" className="rounded-xl" onClick={onToggle}>
                    <AlignJustify color="#666" className="h-5 w-5" />
                </Button>
            </div>
            <div className="flex-1 flex items-center justify-end">
                <DropdownMenu>
                    <DropdownMenuTrigger asChild>
                        <Button variant="outline" size="icon" className="rounded-xl">
                            <Settings color="#666" className="h-5 w-5" />
                        </Button>
                    </DropdownMenuTrigger>
                    <DropdownMenuContent className="w-40">
                        <DropdownMenuLabel>FlowiseAI</DropdownMenuLabel>
                        <DropdownMenuSeparator />
                        <DropdownMenuItem>
                            <User className="mr-2 h-4 w-4" />
                            <span>个人中心</span>
                        </DropdownMenuItem>
                        <DropdownMenuItem>
                            <Info className="mr-2 h-4 w-4" />
                            <span>版本：0.0.1 beta</span>
                        </DropdownMenuItem>
                        <DropdownMenuSeparator />
                        <DropdownMenuItem>
                            <LogOut className="mr-2 h-4 w-4" />
                            <span>退出</span>
                        </DropdownMenuItem>
                    </DropdownMenuContent>
                </DropdownMenu>
            </div>
        </div>
    );
}

