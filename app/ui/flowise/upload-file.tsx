import { Upload } from "lucide-react";
import { useState } from "react";
type UploadProps = {
    value?: string[],
    onChange?: (val: string[]) => void
}
export default function UploadFile({ value, onChange }: UploadProps) {
    const [fileList, setFileList] = useState<string[]>(value || []);
    const onChangeFile = (e: any) => {
        const file = e.target.files[0];
        const list = [...fileList, file.name];
        setFileList(list);
        onChange && onChange(list);
    }
    return <div className="flex flex-col text-sm leading-6 text-gray-600">
        <label htmlFor="file-upload" className="flex items-center justify-center cursor-pointer rounded-full px-3 py-1.5 text-sm text-main border-main border active:opacity-75">
            <Upload className="w-4 mr-1" />
            <span>上传文件</span>
            <input id="file-upload" type="file" accept=".doc,.docx,.pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" className="sr-only" onChange={onChangeFile} />
        </label>
        <div className="flex flex-col gap-1 pt-2">
            {
                fileList.map((item: string) => {
                    return <div className="inline-block text-gray text-sm">{item}</div>
                })
            }
        </div>
    </div>
}