"use client";
import { Input } from "../components/ui/input";
import { Textarea } from "../components/ui/textarea";
import { Switch } from "../components/ui/switch";
import { Select, SelectContent, SelectItem, SelectTrigger, SelectValue } from "../components/ui/select";
import UploadFile from "./upload-file";
import JsonViewDialog from "./json-view-dialog";
import { useEffect, useMemo, useState } from "react";
import { getCredentials, requestNodeLoadMethod } from "@/server/flowise/data";
import { Label } from "../components/ui/label";

type OptionItem = {
    label: string,
    name: string
}

type FormFieldProps = {
    value?: any,
    onChange?: (val: any) => void,
    placeholder?: string,
    type?: string,
    required?: boolean,
    showLabel?: boolean,
    label?: string,
    rows?: number, // string 多行输入框
    step?: number // number 数字框
    credentialNames?: string[] // credential 账户凭证
    loadMethod?: string // asyncOptions 异步获取下拉数据
    options?: OptionItem[] // options 下拉数据
}

export default function FormField({ value, onChange, placeholder, type, showLabel = true, required, rows, step, label, options, credentialNames = [], loadMethod }: FormFieldProps) {
    const [credentials, setCredentials] = useState<OptionItem[]>();
    const [models, setModels] = useState<OptionItem[]>();

    useEffect(() => {
        if (type === 'credential') {
            getCredentials(credentialNames?.[0]).then(res => {
                const records = res.map((i: any) => {
                    return {
                        label: i.name,
                        name: i.id,
                    }
                });
                setCredentials(records);
            });
        }
        if (type === 'asyncOptions') {
            requestNodeLoadMethod({ loadMethod }).then(records => {
                setModels(records);
            });
        }
    }, [type]);

    const renderContent = useMemo(() => {
        if (type === 'string' && !!rows) {
            return <Textarea className='text-sm h-8'
                placeholder={placeholder}
                rows={rows}
                value={value}
                onChange={(e: any) => {
                    onChange && onChange(e.target.value)
                }} />;
        }
        if (type === 'string') {
            return <Input className='text-sm h-8'
                placeholder={placeholder}
                value={value}
                onChange={(e: any) => {
                    onChange && onChange(e.target.value)
                }} />;
        }
        if (type === 'number') {
            return <Input className='text-sm h-8'
                placeholder={placeholder}
                type='number'
                step={step}
                value={value}
                onChange={(e: any) => {
                    onChange && onChange(e.target.value)
                }} />;
        }
        if (type === 'boolean') {
            return <Switch
                checked={value}
                onCheckedChange={onChange} />;
        }
        if (type === 'credential') {
            return <Select
                value={value}
                onValueChange={onChange}>
                <SelectTrigger>
                    <SelectValue placeholder={placeholder} />
                </SelectTrigger>
                <SelectContent>
                    {
                        (credentials || []).map(item => <SelectItem key={item.name} value={item.name}>{item.label}</SelectItem>)
                    }
                </SelectContent>
            </Select>
        }
        if (type === 'asyncOptions') {
            return <Select
                value={value}
                onValueChange={onChange}>
                <SelectTrigger>
                    <SelectValue placeholder={placeholder} />
                </SelectTrigger>
                <SelectContent>
                    {
                        (models || []).map(item => <SelectItem key={item.name} value={item.name}>{item.label}</SelectItem>)
                    }
                </SelectContent>
            </Select>
        }
        if (type === 'options') {
            return <Select
                value={value}
                onValueChange={onChange}>
                <SelectTrigger>
                    <SelectValue placeholder={placeholder} />
                </SelectTrigger>
                <SelectContent>
                    {
                        (options || []).map(item => <SelectItem key={item.name} value={item.name}>{item.label}</SelectItem>)
                    }
                </SelectContent>
            </Select>
        }
        // if (type === 'json') {
        //     return <JsonViewDialog value={value} onChange={onChange} title={label} isDialog={true}>
        //         <div className='cursor-pointer px-3 py-1.5 text-center border rounded-full text-main border-main hover:bg-[#2196f30a]'>{label}</div>
        //     </JsonViewDialog>
        // }
        if (type === 'file') {
            return <UploadFile />
        }
        return <Label className='text-[#666]'>{label}</Label>;
    }, [value, onChange, type, label, rows, step, credentialNames, credentials, models]);

    return <div className='relative flex flex-col gap-2 px-3'>
        {
            showLabel && <div className='flex items-center gap-1 text-sm'>
                <Label className='text-[#666]'>{label}</Label>
                {required && <span className='text-[#f44336]'>*</span>}
            </div>
        }
        {renderContent}
    </div>
}