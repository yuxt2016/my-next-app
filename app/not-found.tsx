'use client';
import Error from 'next/error';

export default function NotFound() {
    return (
        <html lang="en">
            <body>
                <Error statusCode={404} displayName="My-Next-App" title="This page is not found" />
            </body>
        </html>
    );
}

