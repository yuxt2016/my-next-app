
import { createSharedPathnamesNavigation, Pathnames } from 'next-intl/navigation';

export const locales = ['en', 'zh'];
export const localePrefix = "as-needed";
export const defaultLocale = "zh";
export const localeItems = [
    { name: "English", code: "en", iso: "en-US", dir: "ltr" },
    { name: "中文", code: "zh", iso: "zh-CN", dir: "ltr" }
]
// export const pathnames = {
//     '/': '/',
//     '/user': '/user',
//     '/dashboard': '/dashboard',
//     '/nested': {
//         en: '/next-home',
//         zh: '/next-zh-home'
//     },
// } satisfies Pathnames<typeof locales>;

export const { Link, redirect, usePathname, useRouter } = createSharedPathnamesNavigation({ locales, localePrefix });