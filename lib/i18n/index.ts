import { getRequestConfig } from "next-intl/server";
import { notFound } from "next/navigation";
import { locales } from "./config";

// Create this configuration once per request and
// make it available to all Server Components.
export default getRequestConfig(async ({ locale }) => {
    if (!locales.includes(locale as any)) notFound();
    return {
        // Load translations for the active locale.
        messages: (await import(`./locales/${locale}/common.json`)).default,
    }
});