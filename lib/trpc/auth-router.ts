import { z } from "zod";
import { TRPCError } from '@trpc/server';
import type { User } from '@/types/definitions';
import client from "@/server/db/client";
import { publicProcedure, router } from "./trpc";

export const authRouter = router({
    getInfo: publicProcedure.query(async () => {
        // 查询数据库
        const users = await client.$queryRaw<User[]>`SELECT * FROM users`;
        return users;
    }),
    login: publicProcedure
        .input(
            z.object({
                username: z.string(),
                password: z.string(),
            })
        )
        .mutation(async ({ input, ctx }) => {
            try {
                return {
                    code: 200,
                    data: {
                        username: input.username,
                        password: input.password
                    }
                };
            } catch (error: any) {
                throw new TRPCError({
                    code: 'INTERNAL_SERVER_ERROR',
                    message: error.message,
                });
            }
        })
});