import { router } from './trpc';
import { authRouter } from './auth-router';

// 合并路由
export const appRouter = router({
    authRouter
});
// 合并保持同一个命名空间
// export const appRouter = mergeRouters(authRouter)；

export type AppRouter = typeof appRouter;