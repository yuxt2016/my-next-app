import { ReactFlowInstance } from 'reactflow';
import { create } from 'zustand';
import cloneDeep from 'lodash.clonedeep';
import { getUniqueNodeId } from '../utils/help';

interface ReactflowState {
    reactFlowInstance: ReactFlowInstance,
    setReactFlowInstance: (instance: ReactFlowInstance) => void,
    duplicateNode: (id: string) => void,
    deleteNode: (nodeid: string) => void,
    deleteEdge: (edgeid: string) => void
}

// todo 待理解
const deleteConnectedInput = (instance: ReactFlowInstance, id: string, type: string) => {
    const connectedEdges: any[] =
        type === 'node'
            ? instance.getEdges().filter((edge) => edge.source === id)
            : instance.getEdges().filter((edge) => edge.id === id)

    for (const edge of connectedEdges) {
        const targetNodeId = edge.target
        const sourceNodeId = edge.source
        const targetInput = edge.targetHandle.split('-')[2]

        instance.setNodes((nds) =>
            nds.map((node) => {
                if (node.id === targetNodeId) {
                    let value
                    const inputAnchor = node.data.inputAnchors.find((ancr: any) => ancr.name === targetInput)
                    const inputParam = node.data.inputParams.find((param: any) => param.name === targetInput)

                    if (inputAnchor && inputAnchor.list) {
                        const values = node.data.inputs[targetInput] || []
                        value = values.filter((item: any) => !item.includes(sourceNodeId))
                    } else if (inputParam && inputParam.acceptVariable) {
                        value = node.data.inputs[targetInput].replace(`{{${sourceNodeId}.data.instance}}`, '') || ''
                    } else {
                        value = ''
                    }
                    node.data = {
                        ...node.data,
                        inputs: {
                            ...node.data.inputs,
                            [targetInput]: value
                        }
                    }
                }
                return node
            })
        )
    }
}

const useReactflowStore = create<ReactflowState>((set, get) => ({
    reactFlowInstance: get()?.reactFlowInstance,
    setReactFlowInstance: (instance: ReactFlowInstance) => set((state) => ({
        reactFlowInstance: state.reactFlowInstance = instance,
    })),
    duplicateNode: (id: string) => {
        const instance = get()?.reactFlowInstance;
        const nodes = instance.getNodes()
        const originalNode = nodes.find((n) => n.id === id)
        if (originalNode) {
            const newNodeId = getUniqueNodeId(originalNode.data, nodes)
            const clonedNode: any = cloneDeep(originalNode)
            const duplicatedNode = {
                ...clonedNode,
                id: newNodeId,
                position: {
                    x: clonedNode.position.x + 400,
                    y: clonedNode.position.y
                },
                positionAbsolute: {
                    x: clonedNode.positionAbsolute.x + 400,
                    y: clonedNode.positionAbsolute.y
                },
                data: {
                    ...clonedNode.data,
                    id: newNodeId
                },
                selected: false
            }

            const inputKeys = ['inputParams', 'inputAnchors']
            for (const key of inputKeys) {
                for (const item of duplicatedNode.data[key]) {
                    if (item.id) {
                        item.id = item.id.replace(id, newNodeId)
                    }
                }
            }

            const outputKeys = ['outputAnchors']
            for (const key of outputKeys) {
                for (const item of duplicatedNode.data[key]) {
                    if (item.id) {
                        item.id = item.id.replace(id, newNodeId)
                    }
                    if (item.options) {
                        for (const output of item.options) {
                            output.id = output.id.replace(id, newNodeId)
                        }
                    }
                }
            }

            // todo 待理解
            // Clear connected inputs
            for (const inputName in duplicatedNode.data.inputs) {
                if (
                    typeof duplicatedNode.data.inputs[inputName] === 'string' &&
                    duplicatedNode.data.inputs[inputName].startsWith('{{') &&
                    duplicatedNode.data.inputs[inputName].endsWith('}}')
                ) {
                    duplicatedNode.data.inputs[inputName] = ''
                } else if (Array.isArray(duplicatedNode.data.inputs[inputName])) {
                    duplicatedNode.data.inputs[inputName] = duplicatedNode.data.inputs[inputName].filter(
                        (item) => !(typeof item === 'string' && item.startsWith('{{') && item.endsWith('}}'))
                    )
                }
            }

            instance.setNodes([...nodes, duplicatedNode])
        }
    },
    deleteNode: (nodeid: string) => {
        const instance = get()?.reactFlowInstance;
        const edges = instance.getEdges().filter((ns) => ns.source === nodeid || ns.target === nodeid)
        instance.deleteElements({
            nodes: [{ id: nodeid }],
            edges
        });
        // deleteConnectedInput(instance, nodeid, 'node')
        // instance.setNodes(instance.getNodes().filter((n) => n.id !== nodeid))
        // instance.setEdges(instance.getEdges().filter((ns) => ns.source !== nodeid && ns.target !== nodeid));
    },
    deleteEdge: (edgeid: string) => {
        const instance = get()?.reactFlowInstance
        instance.deleteElements({
            edges: [{ id: edgeid }]
        });
        // deleteConnectedInput(instance, edgeid, 'edge')
        // instance.setEdges(instance.getEdges().filter((edge) => edge.id !== edgeid))
    }
})
);

export default useReactflowStore;
