import { create } from 'zustand';

type DialogOptionType = {
    title: string | React.ReactNode,
    content: string | React.ReactNode,
    showOkBtn?: boolean,
    showCancelBtn?: boolean,
    okBtnClassName?: string,
    cancelBtnClassName?: string,
    onOk?: () => boolean,
    onCancel?: () => void,
}
interface DialogState {
    open: boolean,
    showOkBtn?: boolean,
    showCancelBtn?: boolean,
    okBtnClassName?: string,
    cancelBtnClassName?: string,
    title: string | React.ReactNode,
    content: string | React.ReactNode,
    setOpen: (bool: boolean) => void,
    showDialog: (option: DialogOptionType) => void,
    onOk?: () => void,
    onCancel?: () => void,
}

const useDialogStore = create<DialogState>((set, get) => ({
    open: false,
    showOkBtn: true,
    showCancelBtn: true,
    okBtnClassName: "",
    cancelBtnClassName: "",
    title: null,
    content: null,
    setOpen: (bool: boolean) => set({
        open: bool
    }),
    showDialog: (option: DialogOptionType) => set({
        open: true,
        okBtnClassName: option.okBtnClassName,
        cancelBtnClassName: option.cancelBtnClassName,
        title: option.title,
        content: option.content,
        onOk: () => {
            if (option.onOk && option.onOk()) {
                set({ open: false })
            }
        },
        onCancel: () => {
            set({ open: false })
            option.onCancel && option.onCancel()
        }
    }),
    close: () => set({
        open: false
    }),
    onCancel: () => set({
        open: false
    }),
    onOk: () => set({
        open: false
    })
}));

export default useDialogStore;