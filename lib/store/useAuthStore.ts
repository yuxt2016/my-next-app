import { create } from 'zustand';
import { createJSONStorage, persist } from 'zustand/middleware';

interface AuthState {
    user: any;
    token: null | string;
    setToken: (newVal: string) => void;
    login: (email: string, password: string) => Promise<void>;
    logout: () => void;
}

const useAuthStore = create<AuthState>()(
    persist(
        (set) => ({
            user: null,
            token: null,
            setToken: (newVal) => set((state) => ({
                token: state.token = newVal,
            })),
            login: async () => {
                // Login user code
            },
            logout: () => {
                // Logout user code
            },
        }),
        {
            name: 'auth',
            storage: createJSONStorage(() => sessionStorage), // default localstorage
        },
    ),
);

export default useAuthStore;