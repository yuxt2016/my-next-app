/*
 * @Author: yuxt
 * @Date: 2024-04-12 18:47:09
 * @LastEditTime: 2024-05-29 09:41:40
 * @LastEditors: yuxt
 * @Description: 
 */
import { defaultLocale, locales } from '@/lib/i18n/config';
import { create } from 'zustand';
import { createJSONStorage, persist } from 'zustand/middleware';

interface SettingState {
    title: string,
    defaultLocale: string;
    locales: string[];
    setDefaultLocale: (newVal: string) => void;
}

const useSettingStore = create<SettingState>()(
    persist(
        (set, get) => ({
            title: get()?.title || "Next",
            defaultLocale: get()?.defaultLocale ? get()?.defaultLocale : defaultLocale,
            locales: locales,
            setDefaultLocale: (newVal: string) => set((state) => ({
                defaultLocale: state.defaultLocale = newVal,
            })),
        }),
        {
            name: 'setting',
            storage: createJSONStorage(() => sessionStorage), // default localstorage
        },
    ),
);

export default useSettingStore;