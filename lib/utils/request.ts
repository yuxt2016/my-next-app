import { G, requestCreator } from '@siyuan0215/easier-axios-dsl';

const TIMEOUT = {
    DEFAULT: 3 * 60000,
    UPLOADING: 5 * 60000,
};
export const request = requestCreator({
    baseURL: '/',
    timeout: TIMEOUT.DEFAULT,
    withCredentials: true,
    requestInterceptors: [
        (config) => {
            return {
                ...config,
                timeout: TIMEOUT.UPLOADING,
                headers: {
                    ...config.headers,
                },
            };
        },
        (error: any) => Promise.reject(error),
    ],
    responseInterceptors: [
        (response: any) => {
            const { status } = response;

            if (status === 200) {
                return response;
            }
            return Promise.reject(response);
        },
        (error: string) => {
            return Promise.reject(error);
        },
    ],
});

export const generatorAPIS = <T extends {}>(config: T) => G<T>(request, config);
