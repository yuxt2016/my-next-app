import { Revenue } from '@/types/definitions';

export const formatCurrency = (amount: number) => {
  return (Number(amount) / 100).toLocaleString('en-US', {
    style: 'currency',
    currency: 'USD',
  });
};

export const formatDateToLocal = (
  dateStr: string,
  locale: string = 'en-US',
) => {
  const date = new Date(dateStr);
  const options: Intl.DateTimeFormatOptions = {
    day: 'numeric',
    month: 'short',
    year: 'numeric',
  };
  const formatter = new Intl.DateTimeFormat(locale, options);
  return formatter.format(date);
};

export const generateYAxis = (revenue: Revenue[]) => {
  // Calculate what labels we need to display on the y-axis
  // based on highest record and in 1000s
  const yAxisLabels = [];
  const highestRecord = Math.max(...revenue.map((month) => month.revenue));
  const topLabel = Math.ceil(highestRecord / 1000) * 1000;

  for (let i = topLabel; i >= 0; i -= 1000) {
    yAxisLabels.push(`$${i / 1000}K`);
  }

  return { yAxisLabels, topLabel };
};

export const generatePagination = (currentPage: number, totalPages: number) => {
  // If the total number of pages is 7 or less,
  // display all pages without any ellipsis.
  if (totalPages <= 7) {
    return Array.from({ length: totalPages }, (_, i) => i + 1);
  }

  // If the current page is among the first 3 pages,
  // show the first 3, an ellipsis, and the last 2 pages.
  if (currentPage <= 3) {
    return [1, 2, 3, '...', totalPages - 1, totalPages];
  }

  // If the current page is among the last 3 pages,
  // show the first 2, an ellipsis, and the last 3 pages.
  if (currentPage >= totalPages - 2) {
    return [1, 2, '...', totalPages - 2, totalPages - 1, totalPages];
  }

  // If the current page is somewhere in the middle,
  // show the first page, an ellipsis, the current page and its neighbors,
  // another ellipsis, and the last page.
  return [
    1,
    '...',
    currentPage - 1,
    currentPage,
    currentPage + 1,
    '...',
    totalPages,
  ];
};

// 获取url上参数
export function getRequestParams(address?: string) {
  const url = address || window.location.href // 获取url中"?"符后的字串

  const requestParams: Record<string, any> = {}
  if (url.indexOf('?') !== -1) {
      const str = url.substr(url.indexOf('?') + 1)
      const strs = str.split('&')
      for (let i = 0; i < strs.length; i++) {
          requestParams[strs[i].split('=')[0]] = unescape(strs[i].split('=')[1])
      }
  }
  return requestParams
}

export const getUniqueNodeId = (nodeData: any, nodes: any[]) => {
  // Get amount of same nodes
  let totalSameNodes = 0
  for (let i = 0; i < nodes.length; i += 1) {
      const node = nodes[i]
      if (node.data.name === nodeData.name) {
          totalSameNodes += 1
      }
  }

  // Get unique id
  let nodeId = `${nodeData.name}_${totalSameNodes}`
  for (let i = 0; i < nodes.length; i += 1) {
      const node = nodes[i]
      if (node.id === nodeId) {
          totalSameNodes += 1
          nodeId = `${nodeData.name}_${totalSameNodes}`
      }
  }
  return nodeId
}

export const initializeDefaultNodeData = (nodeParams: any) => {
  const initialValues: any = {}

  for (let i = 0; i < nodeParams.length; i += 1) {
      const input = nodeParams[i]
      initialValues[input.name] = input.default || ''
  }

  return initialValues
}

export const initNode = (nodeData: any, newNodeId: any) => {
  const inputAnchors = []
  const inputParams = []
  const incoming = nodeData.inputs ? nodeData.inputs.length : 0
  const outgoing = 1

  const whitelistTypes = [
      'asyncOptions',
      'options',
      'multiOptions',
      'datagrid',
      'string',
      'number',
      'boolean',
      'password',
      'json',
      'code',
      'date',
      'file',
      'folder'
  ]

  // Inputs
  for (let i = 0; i < incoming; i += 1) {
      const newInput = {
          ...nodeData.inputs[i],
          id: `${newNodeId}-input-${nodeData.inputs[i].name}-${nodeData.inputs[i].type}`
      }
      if (whitelistTypes.includes(nodeData.inputs[i].type)) {
          inputParams.push(newInput)
      } else {
          inputAnchors.push(newInput)
      }
  }

  // Credential
  if (nodeData.credential) {
      const newInput = {
          ...nodeData.credential,
          id: `${newNodeId}-input-${nodeData.credential.name}-${nodeData.credential.type}`
      }
      inputParams.unshift(newInput)
  }

  // Outputs
  const outputAnchors = []
  for (let i = 0; i < outgoing; i += 1) {
      if (nodeData.outputs && nodeData.outputs.length) {
          const options = []
          for (let j = 0; j < nodeData.outputs.length; j += 1) {
              let baseClasses = ''
              let type = ''

              const outputBaseClasses = nodeData.outputs[j].baseClasses ?? []
              if (outputBaseClasses.length > 1) {
                  baseClasses = outputBaseClasses.join('|')
                  type = outputBaseClasses.join(' | ')
              } else if (outputBaseClasses.length === 1) {
                  baseClasses = outputBaseClasses[0]
                  type = outputBaseClasses[0]
              }

              const newOutputOption = {
                  id: `${newNodeId}-output-${nodeData.outputs[j].name}-${baseClasses}`,
                  name: nodeData.outputs[j].name,
                  label: nodeData.outputs[j].label,
                  description: nodeData.outputs[j].description ?? '',
                  type
              }
              options.push(newOutputOption)
          }
          const newOutput = {
              name: 'output',
              label: 'Output',
              type: 'options',
              description: nodeData.outputs[0].description ?? '',
              options,
              default: nodeData.outputs[0].name
          }
          outputAnchors.push(newOutput)
      } else {
          const newOutput = {
              id: `${newNodeId}-output-${nodeData.name}-${nodeData.baseClasses.join('|')}`,
              name: nodeData.name,
              label: nodeData.type,
              description: nodeData.description ?? '',
              type: nodeData.baseClasses.join(' | ')
          }
          outputAnchors.push(newOutput)
      }
  }

  /* Initial
  inputs = [
      {
          label: 'field_label_1',
          name: 'string'
      },
      {
          label: 'field_label_2',
          name: 'CustomType'
      }
  ]

  =>  Convert to inputs, inputParams, inputAnchors

  =>  inputs = { 'field': 'defaultvalue' } // Turn into inputs object with default values
  
  =>  // For inputs that are part of whitelistTypes
      inputParams = [
          {
              label: 'field_label_1',
              name: 'string'
          }
      ]

  =>  // For inputs that are not part of whitelistTypes
      inputAnchors = [
          {
              label: 'field_label_2',
              name: 'CustomType'
          }
      ]
  */

  // Inputs
  if (nodeData.inputs) {
      nodeData.inputAnchors = inputAnchors
      nodeData.inputParams = inputParams
      nodeData.inputs = initializeDefaultNodeData(nodeData.inputs)
  } else {
      nodeData.inputAnchors = []
      nodeData.inputParams = []
      nodeData.inputs = {}
  }

  // Outputs
  if (nodeData.outputs) {
      nodeData.outputs = initializeDefaultNodeData(outputAnchors)
  } else {
      nodeData.outputs = {}
  }
  nodeData.outputAnchors = outputAnchors

  // Credential
  if (nodeData.credential) nodeData.credential = ''

  nodeData.id = newNodeId

  return nodeData
}
