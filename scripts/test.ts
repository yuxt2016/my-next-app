// import { ChatOpenAI } from "@langchain/openai";
// import { JsonOutputFunctionsParser } from "langchain/output_parsers";
// import { HumanMessage } from "@langchain/core/messages";


// (async () => {
//     const parser = new JsonOutputFunctionsParser();

//     // 等同于 zodToJsonSchema(z.Object())
//     const extractionFunctionSchema = {
//         name: "extractor",
//         description: "Extracts fields from the input.",
//         parameters: {
//             type: "object",
//             properties: {
//                 tone: {
//                     type: "string",
//                     enum: ["positive", "negative"],
//                     description: "The overall tone of the input",
//                 },
//                 word_count: {
//                     type: "number",
//                     description: "The number of words in the input",
//                 },
//                 chat_response: {
//                     type: "string",
//                     description: "A response to the human's input",
//                 },
//             },
//             required: ["tone", "word_count", "chat_response"],
//         },
//     };

//     const model = new ChatOpenAI({ model: "gpt-4",});

//     const chain = model
//         .bind({
//             functions: [extractionFunctionSchema],
//             function_call: { name: "extractor" },
//         })
//         .pipe(parser);

//     const result = await chain.invoke([
//         new HumanMessage("1+1=?"),
//     ]);

//     console.log({ result });
// })();

import { MemoryVectorStore } from "langchain/vectorstores/memory";
import { OpenAIEmbeddings } from "@langchain/openai";
import { TextLoader } from "langchain/document_loaders/fs/text";

(async () => {
    const loader = new TextLoader("docs/123.txt");
    const docs = await loader.load();
    
    // Load the docs into the vector store
    const vectorStore = await MemoryVectorStore.fromDocuments(
      docs,
      new OpenAIEmbeddings()
    );
    
    // Search for the most similar document
    const resultOne = await vectorStore.similaritySearch("hello world", 1);
    
    console.log(resultOne)
})();
